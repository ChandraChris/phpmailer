<?php
    require_once 'vendor/autoload.php';
    
    $html = '<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>PDF</title>
        <style>
            .table{
                font-size: 0.5em;
            }
            .paragraf{
                font-size: 0.3em;
                margin-left: 5px;
            }
            table, th, td {
                border: 0.3px solid black;
            }
        </style>
    </head>
    
    <body>
    <div style="background-image: url(';$html .="'img/ydsf.jpeg'";$html .= ');
        background-repeat: no-repeat;background-size: cover;height: 1280px;">
            <div style="font-size: 0.8em;padding-top: 20%;padding-left: 10%">
                <p>Yth Bpk/Ibu
                terhormat Sunan</p>
                <p>Kenjeran no 28</p>
                <p>Surabaya</p>
            </div>
        <div style="padding-right: 7%;margin-top: 19.3%;float: right;width: 330px;">
            <table  cellspacing="0" cellpadding="4" align="right" width = "100%">
                <tr>
                    <th class="table">No Reff</th>
                    <th class="table">Tanggal</th>
                    <th class="table">Program</th>
                    <th class="table">Jumlah</th>
                </tr>
                <tr>
                    <td class="table">5630393</td>
                    <td class="table">02-01-2018</td>
                    <td class="table">Kerjasama Pesantren Anak Sholeh Gontor</td>
                    <td class="table">Rp. 50.000,00</td>
                </tr>
                <tr>
                    <td class="table">5630393</td>
                    <td class="table">02-01-2018</td>
                    <td class="table">Rumah Cinta Yatim</td>
                    <td class="table">Rp. 50.000,00</td>
                </tr>
                <tr>
                    <td class="table" colspan="4" border="none">Total : Rp. 750.000,00</td>
                </tr>
            </table>
        </div>
        <div style="float: right;width: 238px;margin-right: 20px;">
            <img src="img/ydsf.jpeg" alt="YDSF" width="65px" height="100px" style="float:left;">
            <p class="paragraf">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem quaerat eum excepturi,
                praesentium beatae dolorum ab amet ipsum aliquid nobis quibusdam sed perspiciatis eveniet, adipisci quo
                cum illum. Voluptatibus, quisquam. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime
                numquam quis alias suscipit, voluptas magnam perspiciatis fugit earum ipsum est laborum harum illo
                veritatis quasi excepturi ad officia impedit deserunt! Lorem ipsum dolor sit amet consectetur,
                adipisicing elit. Assumenda aliquam sit similique esse ducimus, iste et, placeat, ad odio natus eum
                incidunt consequuntur. Ex numquam error magni, quod iure libero. Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Possimus sint ut nesciunt in aut unde alias a aperiam. Quidem voluptatum quaerat,
                aliquam sunt atque magnam quam maiores doloremque officia quo! Lorem ipsum dolor sit amet, consectetur
                adipisicing elit. Sint repellat ullam, minima rem necessitatibus soluta fugiat voluptate labore
                incidunt dolor sapiente. Aliquam quam voluptate quisquam voluptatibus consectetur quibusdam est itaque?
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam enim ex asperiores quia quaerat, vitae
                totam illo veniam magni soluta tenetur quae aliquam iusto voluptates vero corporis esse! Corrupti,
                pariatur? Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam dicta reiciendis
                praesentium, illo recusandae, culpa tempora consequatur ducimus iste voluptas libero facilis fuga. Ex
                repellendus eaque dignissimos maxime praesentium vitae. Lorem ipsum, dolor sit amet consectetur
                adipisicing elit. Maxime, deleniti porro saepe vitae molestias, sequi doloribus sit eligendi ad
                laudantium culpa eos temporibus. Adipisci aut temporibus repudiandae vero vel facere? Lorem, ipsum
                dolor sit amet consectetur adipisicing elit. Excepturi consequuntur ipsa obcaecati laudantium nostrum
                facere praesentium eaque quidem voluptas odit non, aperiam sint perspiciatis beatae eum corporis hic
                itaque ratione?</p>
        </div>
    </div>
    </body>
    
    </html>';
    $html2 = '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>ydsf2</title>
    </head>
    <body>
        <div style="background-image: url(';$html2 .="'img/ydsf2.jpeg'";$html2 .= ');
            background-repeat: no-repeat;background-size: cover;height: 1280px;width: 905px;">
        </div>
    </body>
    </html>';
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML($html);
    $mpdf->addPage();
    $mpdf->WriteHtml($html2);
    return $mpdf->Output('ydsf.pdf', 'S');
?>